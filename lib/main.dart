import 'dart:developer';

import 'package:asynchronous/services/mockapi.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(title: 'Asynchronous calls'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<int> widthN = [0, 0, 0];
  List<int> numbers = [0, 0, 0];
  List metthod = [
    MockApi().getFerrariInteger(),
  ];

  status(index) async => {
        widthN[index] = 200,
        if (index == 0)
          {
            await MockApi().getFerrariInteger().then((value) => {
                  setState(() {
                    numbers[index] = value;
                  }),
                  widthN[index] = 0
                })
          }
        else if (index == 1)
          {
            await MockApi().getHyundaiInteger().then((value) => {
                  setState(() {
                    numbers[index] = value;
                  }),
                  widthN[index] = 0
                })
          }
        else
          {
            await MockApi().getFisherPriceInteger().then((value) => {
                  setState(() {
                    numbers[index] = value;
                  }),
                  widthN[index] = 0
                })
          }
      };

  @override
  Widget build(BuildContext context) {
    animatedContainer(time, color, index) => AnimatedContainer(
          duration: Duration(seconds: time),
          width: widthN[index].toDouble(),
          height: 8,
          decoration: BoxDecoration(color: color),
        );

    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(widget.title)),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.green,
                        shape: CircleBorder(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: IconButton(
                          icon: const Icon(
                            Icons.flash_on,
                            color: Colors.white,
                          ),
                          iconSize: 25,
                          onPressed: () {
                            setState(() {
                              status(0);
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: animatedContainer(1, Colors.green, 0),
                    ),
                    Text(
                      numbers[0].toString(),
                      style: const TextStyle(
                        fontSize: 40,
                        color: Colors.green,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.orange,
                        shape: CircleBorder(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: IconButton(
                          icon: const Icon(
                            Icons.airport_shuttle,
                            color: Colors.white,
                          ),
                          iconSize: 25,
                          onPressed: () {
                            setState(() {
                              status(1);
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: animatedContainer(3, Colors.orange, 1),
                    ),
                    Text(
                      numbers[1].toString(),
                      style: const TextStyle(
                        fontSize: 40,
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.red,
                        shape: CircleBorder(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: IconButton(
                          icon: const Icon(
                            Icons.directions_walk,
                            color: Colors.white,
                          ),
                          iconSize: 25,
                          onPressed: () {
                            setState(() {
                              status(2);
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: animatedContainer(10, Colors.red, 2),
                    ),
                    Text(
                      numbers[2].toString(),
                      style: const TextStyle(
                        fontSize: 40,
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
